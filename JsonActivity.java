//JsonActivity.java
package com.v3;
 
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
 
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.CoreProtocolPNames;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONStringer;
 
import android.app.Activity;
import android.os.Bundle;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
 
public class JsonActivity extends Activity {
Button button,buttonClear;
TextView textView;
String page;
String str="";
 
String url = "http://192.168.1.118/androidjsontest/index.php";
/** Called when the activity is first created. */
@Override
public void onCreate(Bundle savedInstanceState) {
super.onCreate(savedInstanceState);
setContentView(R.layout.main);
button = (Button) findViewById(R.id.btnFetch);
buttonClear = (Button) findViewById(R.id.btnClear);
textView = (TextView) findViewById(R.id.txtView);
button.setOnClickListener(new Button.OnClickListener()
{
public void onClick(View v)
{
examineJSONFile();
}
});
buttonClear.setOnClickListener(new Button.OnClickListener()
{
public void onClick(View v)
{
textView.setText("");
}
});
 
}
 
 
public String executeHttpGet(String URL) throws Exception
{
//This method for HttpConnection
BufferedReader bufferedReader = null;
try
{
HttpClient client = new DefaultHttpClient();
client.getParams().setParameter(CoreProtocolPNames.USER_AGENT, "android");
HttpGet request = new HttpGet();
request.setHeader("Content-Type", "text/plain; charset=utf-8");
request.setURI(new URI(URL));
HttpResponse response = client.execute(request);
bufferedReader = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
 
StringBuffer stringBuffer = new StringBuffer("");
String line = "";
 
String NL = System.getProperty("line.separator");
while ((line = bufferedReader.readLine()) != null)
{
stringBuffer.append(line + NL);
System.out.print(stringBuffer);
}
bufferedReader.close();
page = stringBuffer.toString();
System.out.println(page+"page");
return page;
}
finally
{
if (bufferedReader != null)
{
try
{
bufferedReader.close();
}
catch (IOException e)
{
Log.d("BBB", e.toString());
}
}
}
}
 
 
void examineJSONFile()
{
try
{
 
String Value="" ;
String str = "";
page = executeHttpGet(url);
Log.i("hello", str.toString());
 
JSONObject object=new JSONObject(page);
str=str+object.names().toString();
 
String sub1=str.substring(2, str.length()-2);
String[] names=sub1.split("\",\"");
 
for (int i=0;i<names.length;i++) {
Value=Value+names[i] +":"+object.get(names[i])+"\n";
System.out.println(names[i]);
 
}
 
/* String sub=str.substring(1, str.length()-1);
  String[] names=sub.split(",");
  String[] keyValue=new String[names.length];
 
  for (int i=0;i<names.length;i++) {
  names[i]=names[i].replace("\"","");
  Value=Value+names[i] +":"+object.get(names[i])+"\n";
  //keyValue[i]=names[i] +":"+object.get(names[i])+"\n";
System.out.println(names[i]);
 
}*/
 
textView.setText(Value);
 
/*for(int i=0;i<keyValue.length;i++){
  textView.setText(keyValue[i]);
  }*/
 
Log.i("hello", str.toString());
 
}
catch (Exception je)
{
textView.setText("Error w/file: " + je.getMessage());
}
}
 
}